provider "azurerm" {
  features {

  }
}

module "resource_group" {
  source              = "./modules/resource_group_and_location"
  resource_group_name = var.resource_group_name
  location            = var.location
}

module "vnet" {
  source              = "./modules/network/vnet"
  location            = var.location
  resource_group_name = var.resource_group_name

  for_each = var.vnet_config

  vnet_name         = each.value.name
  cidr_address_vnet = each.value.cidr
  depends_on        = [module.resource_group]
}

module "subnet" {
  source              = "./modules/network/subnet"
  resource_group_name = var.resource_group_name

  for_each = var.subnet_config

  subnet_name    = each.value.name
  vnet_name      = each.value.vnet_name
  subnet_address = each.value.network_addresses
  depends_on     = [module.vnet]

}


