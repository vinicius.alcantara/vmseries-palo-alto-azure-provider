variable "location" {
  type        = string
  description = "Define a região"
  default     = "East US 2"
}

variable "resource_group_name" {
  type        = string
  description = "Nome do resource group"
  default     = "infra-devops"
}

variable "rg" {
  type        = string
  description = "Nome do resource group"
  default     = "infra-devops"
}

variable "tag_project" {
  type        = map(any)
  description = "Tag default"
  default = {
    name   = "infra-devops"
    area   = "comercial"
    author = "vinícius"
  }
}

variable "vnet_config" {
  description = "Configuration for virtual networks"
  type = map(object({
    name : string
    cidr : list(string)
  }))
  default = {
    "vnet1" = {
      name = "devops"
      cidr = ["192.168.0.0/16", "10.0.0.0/16"]
    }
    "vnet2" = {
      name = "sre"
      cidr = ["192.168.0.0/16", "172.16.0.0/16"]
    }
  }
}

variable "subnet_config" {
  description = "Configuration subnets"
  type = map(object({
    name : string
    vnet_name : string
    network_addresses : list(string)
  }))
  default = {
    "subnet1" = {
      name : "devops1"
      vnet_name : "devops"
      network_addresses : ["192.168.1.0/24"]
    }
    "subnet2" = {
      name : "sre1"
      vnet_name : "sre"
      network_addresses : ["172.16.1.0/24"]
    }
  }
}