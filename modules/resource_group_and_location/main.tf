resource "azurerm_resource_group" "infra-devops" {
  name     = var.resource_group_name
  location = var.location
}