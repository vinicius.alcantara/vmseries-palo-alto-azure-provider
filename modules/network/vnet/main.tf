resource "azurerm_virtual_network" "vnet" {
  name                = var.vnet_name
  address_space       = var.cidr_address_vnet
  resource_group_name = var.resource_group_name
  location            = var.location
}
