variable "vnet_name" {
  type        = string
  description = "nome da vnet"
}

variable "cidr_address_vnet" {
  type = list(string)
}

variable "resource_group_name" {
  type = string
}

variable "location" {
  type = string
}


