variable "subnet_name" {
  type = string
}

variable "subnet_address" {
  type = list(string)
}

variable "vnet_name" {
  type = string
}

variable "resource_group_name" {
  type = string
}