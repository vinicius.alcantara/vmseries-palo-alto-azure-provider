resource "azurerm_subnet" "subnets" {
  name                 = var.subnet_name
  address_prefixes     = var.subnet_address
  virtual_network_name = var.vnet_name
  resource_group_name = var.resource_group_name
}